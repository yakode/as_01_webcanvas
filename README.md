# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |


---

### How to use 

![](https://i.imgur.com/iSwtBPu.png) Brush: 按下鼠標就會畫出移動軌跡

![](https://i.imgur.com/d31nqCM.png) Eraser: 按下鼠標就會把軌跡上的筆畫擦掉

![](https://i.imgur.com/w9lk5Ey.png) Line:按下鼠標定下直線的一端，放開的點會是直線的另外一端

![](https://i.imgur.com/WdnXCXd.png) Rectangle: 按下鼠標定下矩形的一角，放開的點會是對角，畫出矩形

![](https://i.imgur.com/j3kIY8t.png) Triangle: 按下鼠標定下三角型的一個頂點，放開就畫出三角型

![](https://i.imgur.com/IKthJFA.png) Circle: 畫出圓形，點下鼠標的位置是圓的中心

![](https://i.imgur.com/tFLw8lE.png) Text: 點兩下出現輸入框，輸入完字點輸入框外，就可以在畫布上寫字

![](https://i.imgur.com/V00hF1C.png) Undo: 回到上一步(還原)

![](https://i.imgur.com/tJFE61j.png) Redo: 取消還原

![](https://i.imgur.com/IJptRvD.png) Upload: 上傳圖片(會先清空畫布再蓋上上傳的圖)

![](https://i.imgur.com/o2rZxt3.png) Download: 下載畫布中的圖

![](https://i.imgur.com/3KehTp8.png) Refresh: 清空畫布

![](https://i.imgur.com/pSxH0HJ.png) Color selector: 點選任一點取得顏色

![](https://i.imgur.com/6PBiRau.png) Size bar:移動bar，改變brush/text的size

![](https://i.imgur.com/C6xDdfc.png) Font select: 點選清單內的字型，更改Text工具的字型





### Gitlab page link

https://yakode.gitlab.io/AS_01_WebCanvas/



<style>
table th{
    width: 100%;
}
</style>